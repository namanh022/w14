import React , { Component}  from 'react';
import './App.css';

function App() {
  //here is an example how to use localStorage, see https://www.w3schools.com/jsref/prop_win_localstorage.asp

  //function reads data from localStorage, before the read you need to write something
  const lists = [];
  function show(){
    const myValueInLocalStorage = localStorage.getItem("chats");
    lists.push(JSON.parse(myValueInLocalStorage))
    console.log(lists);
  }

  //function writes data to localStorage
  function send(){
    localStorage.setItem("chats",JSON.stringify(document.getElementById("example").value));
  }

  return (
    <div className="App">
      {/* this is just example implement according to the given task */}
      <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Montes nascetur ridiculus mus mauris vitae ultricies. Amet tellus cras adipiscing enim eu turpis egestas pretium aenean. Eu feugiat pretium nibh ipsum consequat nisl. Tristique senectus et netus et malesuada fames ac. Massa enim nec dui nunc mattis enim ut tellus. Quis imperdiet massa tincidunt nunc pulvinar sapien. Cursus euismod quis viverra nibh cras pulvinar mattis nunc. Tellus molestie nunc non blandit massa enim. Commodo viverra maecenas accumsan lacus vel facilisis volutpat est. Facilisi etiam dignissim diam quis enim lobortis scelerisque. Scelerisque fermentum dui faucibus in ornare quam viverra orci sagittis. Ut tortor pretium viverra suspendisse potenti nullam ac tortor.</p>

<p>Pellentesque sit amet porttitor eget dolor morbi non. In nisl nisi scelerisque eu ultrices. Commodo viverra maecenas accumsan lacus vel facilisis volutpat. Turpis in eu mi bibendum neque egestas congue quisque. Integer quis auctor elit sed vulputate mi. Fermentum odio eu feugiat pretium nibh ipsum. Diam maecenas sed enim ut sem viverra aliquet. Eget sit amet tellus cras adipiscing enim eu. Lacus laoreet non curabitur gravida arcu ac tortor dignissim convallis. Lectus sit amet est placerat in egestas erat imperdiet. Dictum sit amet justo donec enim diam vulputate. Nibh ipsum consequat nisl vel pretium lectus. Massa tincidunt dui ut ornare lectus sit amet est placerat. Vitae proin sagittis nisl rhoncus mattis.</p>

<p>Odio tempor orci dapibus ultrices in. Feugiat nisl pretium fusce id velit. Odio tempor orci dapibus ultrices in iaculis nunc sed. Quis auctor elit sed vulputate mi. Fusce ut placerat orci nulla pellentesque dignissim. Sit amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar. Vulputate sapien nec sagittis aliquam. Ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat. Lorem mollis aliquam ut porttitor leo a diam sollicitudin. Est placerat in egestas erat imperdiet sed euismod.</p>

<p>Aenean euismod elementum nisi quis eleifend quam adipiscing. Sed libero enim sed faucibus turpis in eu. Fames ac turpis egestas integer eget. Dolor magna eget est lorem. Augue ut lectus arcu bibendum at varius vel pharetra. Viverra tellus in hac habitasse platea dictumst vestibulum rhoncus est. Montes nascetur ridiculus mus mauris vitae ultricies leo. Amet consectetur adipiscing elit duis. Leo integer malesuada nunc vel risus commodo viverra maecenas. Vitae auctor eu augue ut lectus arcu. Tempor id eu nisl nunc mi ipsum. Quam adipiscing vitae proin sagittis.</p>

<p>Sit amet risus nullam eget felis eget nunc. Adipiscing elit duis tristique sollicitudin nibh sit amet commodo nulla. Nunc mattis enim ut tellus elementum. Hendrerit dolor magna eget est lorem ipsum dolor sit. Est pellentesque elit ullamcorper dignissim cras. Et egestas quis ipsum suspendisse ultrices gravida dictum. Sed viverra tellus in hac habitasse platea dictumst. Nec nam aliquam sem et tortor consequat id. Tristique sollicitudin nibh sit amet commodo nulla facilisi nullam. Aliquam faucibus purus in massa tempor. Luctus venenatis lectus magna fringilla urna porttitor.</p>
    
      <div class="form_chat">
      <input type="text" id="example"></input>
      <button onClick={send}>Send</button>
      <button onClick={show}>Show messages</button>
      
      </div>
    </div>
  );
}

export default App;
